from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class CareerStaticViewSitemap(Sitemap):
    def items(self):
        return ["career"]

    def location(self, item):
        return reverse(item)
