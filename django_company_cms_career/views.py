from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)


def home(request):
    template = loader.get_template("django_company_cms_career/home.html")

    template_opts = dict()

    company_name = CompanyCmsGeneral.objects.get(name="company_name")

    template_opts["content_title_main"] = company_name.content
    template_opts["content_title_sub"] = _("Career")

    return HttpResponse(template.render(template_opts, request))
